# Hello, world! 

This Web page is written in [Markdown](https://www.markdowntutorial.com/) and will be converted to HTML in a GitLab CI pipeline. You can view the source or **Fork** it and make it your own at:

  - [https://gitlab.com/brie/intro-git-ci](https://gitlab.com/brie/intro-git-ci)

The HTML that this file is generated to is served at [https://intro.brie.dev](https://intro.brie.dev). 


## My cat, Plop  

I have a cat named Plop and I love him very much. He is super duper cute.  He is three years old. Like most cats, he likes to sit in cardboard boxes and on any kind of paper that crinkles. He likes to sit in large boxes more than anything. Here are some of the other things that Plop likes:

  - looking out the window
  - doing tricks for treats
  - playing with yarn
  - catnip mice toys
  - fresh catgrass

Here's a photo of Plop: 

![](https://io.brie.ninja/plop.jpg)



---


> For there is always light,
> if only we're brave enough to see it.
> If only we're brave enough to be it.
>
> 		-- Amanda Gorman
